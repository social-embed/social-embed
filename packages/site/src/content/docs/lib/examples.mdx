---
id: examples
title: Examples
description: How to parse and embed various media URLs with @social-embed/lib
sidebar:
  order: 25
---

@social-embed/lib unifies URL detection and parsing for various services (YouTube, Vimeo, Spotify, etc.). It offers both:

1. **Universal** – using `convertUrlToEmbedUrl(url)` to handle most providers automatically.
2. **Provider-specific** – extracting IDs manually with functions like `getYouTubeIdFromUrl`, `getDailyMotionIdFromUrl`, etc.

## Quick Overview

| Function                       | Description                                                                                                                                         |
|--------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| `convertUrlToEmbedUrl(url)`    | Converts a recognized media URL into its `<iframe>`-friendly URL. Returns `""` if unrecognized or invalid.                                         |
| `isValidUrl(url)`              | Checks if the string is a syntactically valid URL.                                                                                                 |
| `getYouTubeIdFromUrl(url)`     | Extracts a YouTube video ID (e.g. `FTQbiNvZqaY`) from various YouTube link forms.                                                                  |
| `getYouTubeEmbedUrlFromId(id)` | Builds `https://www.youtube.com/embed/<id>`                                                                                                        |
| … (DailyMotion, Spotify, etc.) | Similar patterns for DailyMotion, Spotify, Vimeo, Loom, Wistia, EdPuzzle, etc. See details below.                                                  |

---

## 1. Universal Conversion

### `convertUrlToEmbedUrl(url)`

```ts
import { convertUrlToEmbedUrl } from "@social-embed/lib";

const embedUrl = convertUrlToEmbedUrl("https://youtu.be/FTQbiNvZqaY");
console.log(embedUrl);
// "https://www.youtube.com/embed/FTQbiNvZqaY"
```

- **What it does**  
  - Detects the provider (YouTube, Vimeo, Spotify, Loom, etc.) by testing known URL patterns  
  - Extracts the necessary ID(s)  
  - Returns a fully embeddable URL (e.g. `https://www.youtube.com/embed/FTQbiNvZqaY`)

- **Invalid or Unrecognized**  
  - If the URL is **valid** but from an unknown service, `convertUrlToEmbedUrl` returns the **normalized** URL or `""` depending on the version of the library.  
  - If the string is not a valid URL at all, it returns `""`.

#### Example: Spotify

```ts
console.log(convertUrlToEmbedUrl("spotify:album:1DFixLWuPkv3KT3TnV35m3"));
// "https://open.spotify.com/embed/album/1DFixLWuPkv3KT3TnV35m3"
```

#### Example: DailyMotion

```ts
console.log(convertUrlToEmbedUrl("http://dailymotion.com/video/x7znrd0"));
// "https://www.dailymotion.com/embed/video/x7znrd0"
```

#### Example: Vimeo

```ts
console.log(convertUrlToEmbedUrl("vimeo.com/channels/staffpicks/134668506"));
// "https://player.vimeo.com/video/134668506"
```

#### Example: EdPuzzle

```ts
console.log(
  convertUrlToEmbedUrl("edpuzzle.com/media/606b413369971e424ec6021e"),
);
// "https://edpuzzle.com/embed/media/606b413369971e424ec6021e"
```

---

## 2. URL Validation

### `isValidUrl(url)`

```ts
import { isValidUrl } from "@social-embed/lib";

console.log(isValidUrl("https://apple.com")); // true
console.log(isValidUrl("notaurl"));           // false
```

- **What it does**  
  - Uses `new URL()` internally to verify if the string is a syntactically valid URL.  
  - Returns `true` or `false`.

---

## 3. Provider-Specific Functions

If you need more control, each provider offers a “get ID” and “build embed URL” approach. For example:

### YouTube

- **`getYouTubeIdFromUrl(url: string): string`**  
  Extracts a short ID (e.g. `"FTQbiNvZqaY"`) from YouTube links.  
- **`getYouTubeEmbedUrlFromId(id: string): string`**  
  Builds `"https://www.youtube.com/embed/<id>"`.

**Example**:

```ts
import {
  getYouTubeIdFromUrl,
  getYouTubeEmbedUrlFromId,
} from "@social-embed/lib";

const id = getYouTubeIdFromUrl("https://youtu.be/FTQbiNvZqaY");
// "FTQbiNvZqaY"

const embedUrl = getYouTubeEmbedUrlFromId(id);
// "https://www.youtube.com/embed/FTQbiNvZqaY"
```

### DailyMotion

```ts
import {
  getDailyMotionIdFromUrl,
  getDailyMotionEmbedFromId,
} from "@social-embed/lib";

const dmId = getDailyMotionIdFromUrl("dailymotion.com/video/x7znrd0");
// "x7znrd0"

const embedUrl = getDailyMotionEmbedFromId(dmId);
// "https://www.dailymotion.com/embed/video/x7znrd0"
```

### Vimeo

```ts
import {
  getVimeoIdFromUrl,
  getVimeoEmbedUrlFromId,
} from "@social-embed/lib";

const vimeoId = getVimeoIdFromUrl("vimeo.com/channels/staffpicks/134668506");
// "134668506"

const embedUrl = getVimeoEmbedUrlFromId(vimeoId);
// "https://player.vimeo.com/video/134668506"
```

### Loom

```ts
import { getLoomIdFromUrl, getLoomEmbedUrlFromId } from "@social-embed/lib";

const loomId = getLoomIdFromUrl(
  "loom.com/share/e883f70b219a49f6ba7fbeac71a72604",
);
// "e883f70b219a49f6ba7fbeac71a72604"

const embedUrl = getLoomEmbedUrlFromId(loomId);
// "https://www.loom.com/embed/e883f70b219a49f6ba7fbeac71a72604"
```

### EdPuzzle

```ts
import {
  getEdPuzzleIdFromUrl,
  getEdPuzzleEmbedUrlFromId,
} from "@social-embed/lib";

const edPuzzleId = getEdPuzzleIdFromUrl(
  "edpuzzle.com/media/606b413369971e424ec6021e",
);
// "606b413369971e424ec6021e"

const embedUrl = getEdPuzzleEmbedUrlFromId(edPuzzleId);
// "https://edpuzzle.com/embed/media/606b413369971e424ec6021e"
```

### Wistia

```ts
import {
  getWistiaIdFromUrl,
  getWistiaEmbedUrlFromId,
} from "@social-embed/lib";

const wistiaId = getWistiaIdFromUrl(
  "https://support.wistia.com/medias/26sk4lmiix",
);
// "26sk4lmiix"

const embedUrl = getWistiaEmbedUrlFromId(wistiaId);
// "https://fast.wistia.net/embed/iframe/26sk4lmiix"
```

---

## 4. `getProviderFromUrl(url)`

If you’d like to detect the provider without immediately converting the URL, call **`getProviderFromUrl(url)`**. It returns an **object** with `.name`, `.getIdFromUrl()`, and `.getEmbedUrlFromId()` methods:

````ts
import { getProviderFromUrl } from "@social-embed/lib";

const provider = getProviderFromUrl("https://youtu.be/FTQbiNvZqaY");
/*
{
  name: "YouTube",
  canParseUrl: [Function],
  getIdFromUrl: [Function],
  getEmbedUrlFromId: [Function]
}
*/

if (provider) {
  // Extract the video ID or IDs
  const extractedId = provider.getIdFromUrl("https://youtu.be/FTQbiNvZqaY");
  // => "FTQbiNvZqaY"

  // Build the final embed URL
  const embedUrl = provider.getEmbedUrlFromId(extractedId);
  // => "https://www.youtube.com/embed/FTQbiNvZqaY"

  console.log(`Embed URL: ${embedUrl}`);
} else {
  console.log("No recognized provider!");
}
````

- **If** `getProviderFromUrl(url)` returns `undefined`, it means no known provider was detected.

---

## 5. Adding a New Provider

If you have a custom service (e.g. **MyCustom**), you can implement your own detection and embed logic.

1. **Implement** an object with these fields:
   ```ts
   export const MyCustomProvider: EmbedProvider = {
     name: "MyCustom", // a unique string name
     canParseUrl(url: string): boolean {
       return /mycustom\.example\.com\/video\//.test(url);
     },
     getIdFromUrl(url: string): string {
       // e.g. last path segment is the ID
       return url.split("/").pop() || "";
     },
     getEmbedUrlFromId(id: string, ...args: unknown[]): string {
       // Return a valid embed URL
       return `https://mycustom.example.com/embed/${id}`;
     },
   };
   ```

2. **Register** it in a registry, or if you’re using `convertUrlToEmbedUrl`, add your provider to the **default registry** (in `@social-embed/lib` code or your own code):
   ```ts
   import { defaultRegistry } from "@social-embed/lib/registry";
   import { MyCustomProvider } from "./MyCustomProvider";

   defaultRegistry.register(MyCustomProvider);

   // Now convertUrlToEmbedUrl() can parse MyCustom URLs.
   console.log(
     convertUrlToEmbedUrl("https://mycustom.example.com/video/xyz123"),
   );
   // "https://mycustom.example.com/embed/xyz123"
   ```

3. **Done**. The library will detect “MyCustom” for your custom URL pattern. If you prefer, you can keep a separate registry if you don’t want to modify the default one.

---

## Summary

- **Universal** approach: `convertUrlToEmbedUrl(url)`.  
- **Manual** approach: provider-specific ID extraction + embed link building.  
- **Custom providers**: add them via the registry or use your own logic.  

If you run into any issues or want to extend coverage (e.g., new services like `Bandcamp`, `SoundCloud`), feel free to open a PR or an issue.

Check out the [tests on GitHub](https://github.com/social-embed/social-embed) or the [Playground](/lib/playground/) for more real-world usage.